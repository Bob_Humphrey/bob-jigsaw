<?php
$dotenv = Dotenv\Dotenv::create(__DIR__);
$dotenv->load();

return [
    'production' => true,
    'baseUrl' => 'https://bob-humphrey.com'
];
