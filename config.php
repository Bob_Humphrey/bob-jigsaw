<?php
$dotenv = Dotenv\Dotenv::create(__DIR__);
$dotenv->load();

return [
    'production' => false,
    'baseUrl' => '',
    'cockpit_token' => function($page) {
        $cockpit_token = getenv('COCKPIT_TOKEN');
        return $cockpit_token;
    },
    'directus_token' => function($page) {
        $directus_token = getenv('DIRECTUS_TOKEN');
        return $directus_token;
    },
    'form_endpoint' => function($page) {
        $form_endpoint = getenv('FORM_ENDPOINT');
        return $form_endpoint;
    },
    'collections' => [
        'projects' => [
            'items' => function() {
                $cockpit_token = getenv('COCKPIT_TOKEN');
                $projectsApiUrl = 'https://cockpit.bob-humphrey.com/api/collections/get/randall?token=' . $cockpit_token;
                $data = json_decode(file_get_contents($projectsApiUrl));
                $projects = collect($data->entries)->map(function ($project) {
                    return [
                        'name' => $project->name,
                        'description' => $project->description,
                        'sort' => $project->sort,
                        'image' => $project->image->path,
                    ];
                });
                return $projects;  
            },
            'sort' => 'sort',
        ],
        'photos' => [
            'extends' => '_layouts.photo',
            'path' => 'photo/{sort}',
            'items' => function() {
                $cockpit_token = getenv('COCKPIT_TOKEN');
                $photosApiUrl = 'https://cockpit.bob-humphrey.com/api/collections/get/photos?token=' . $cockpit_token;
                $data = json_decode(file_get_contents($photosApiUrl));
                $photos = collect($data->entries)->map(function ($photo) {
                    return [
                        'description' => $photo->description,
                        'sort' => $photo->sort,
                        'image' => $photo->image->path,
                    ];
                });
                return $photos;  
            },
            'sort' => 'sort',
        ],
        'paintings' => [
            'items' => function() {
                $cockpit_token = getenv('COCKPIT_TOKEN');
                $paintingsApiUrl = 'https://cockpit.bob-humphrey.com/api/collections/get/paintings?token=' . $cockpit_token;
                $data = json_decode(file_get_contents($paintingsApiUrl));
                $paintings = collect($data->entries)->map(function ($painting) {
                    return [
                        'description' => $painting->description,
                        'sort' => $painting->sort,
                        'image' => $painting->image->path,
                    ];
                });
                return $paintings;  
            },
            'sort' => 'sort',
        ],
        'recent_work' => [
            'items' => function() {
                $directus_token = getenv('DIRECTUS_TOKEN');
                $recentsApiUrl = 'https://directus2.bob-humphrey.com/bob/items/recent_work?'
                . 'access_token=' 
                . $directus_token
                . '&fields=sort,name,url,description,tags,code_url,image_credit,image_credit_url,image.data.*';
                $data = json_decode(file_get_contents($recentsApiUrl));
                $items = collect($data->data)->map(function ($item) {
                    return [
                        'sort' => $item->sort,
                        'name' => $item->name,
                        'url' => $item->url,
                        'description' => $item->description,
                        'tags' => $item->tags,
                        'code_url' => $item->code_url,
                        'image' => $item->image->data->full_url,
                        'thumbnail' => $item->image->data->thumbnails[0]->url,
                        'dog_credit' => $item->image_credit,
                        'dog_credit_url' => $item->image_credit_url
                    ];
                });
                return $items;  
            },
            'sort' => 'sort',
        ],
    ],
];
