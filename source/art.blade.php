---
pagination:
  collection: paintings
  perPage: 20
---

@extends('_layouts.master')

@php

@endphp

@section('pageTitle')
    Art 
@endsection

@section('content')

    <div class="mb-20">

        <nav class="lg:pr-32">
            @include('_partials.page-nav')
        </nav>

        <ul>
            @foreach ($pagination->items as $painting)
                <li class="mb-6 lg:pr-32">
                    <figure class="image">
                        <img src="https://cockpit.bob-humphrey.com/storage/uploads{{ $painting->image }}" alt="{{ $painting->description }}">
                    </figure>
                    <div class="mt-2 mb-10 font-serif text-grey-700 text-lg">
                        {{ $painting->description }}
                    </div>
                </li>
            @endforeach
        </ul>

        <nav class="lg:pr-32">
            @include('_partials.page-nav')
        </nav>

    </div>


@endsection