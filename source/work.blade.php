@extends('_layouts.master')

@section('pageTitle')
Recent Work
@endsection

@section('content')

<div class="text-grey-700 text-lg mb-20">

    <p class="font-serif text-grey-700 text-lg mb-8">
        Personal projects I recently completed.
    </p>

    <div class="">

        @foreach ($recent_work as $recent)

        @php
        $tagCollection = collect($recent->tags);
        $tags = $tagCollection->filter(function ($value, $key) {
        return strlen($value) > 1;
        });
        @endphp

        <div class="mb-8">
            <h3 class="text-blue-500 font-bold text-2xl pb-2">
                {{ $recent->name }}
            </h3>
            <div class="font-serif pb-4">
                {{ $recent->description }}
            </div>
            <div class="flex justify-start ">
                @foreach ($tags as $tag)
                <div class="text-xs text-center border border-grey-600 rounded px-2 py-1 mr-2 mb-4">
                    {{ $tag }}
                </div>
                @endforeach
            </div>
            <div class="flex justify-start text-xs">
                <div class="text-white text-center bg-grey-600 hover:bg-black rounded px-2 py-1 mr-2 mb-4">
                    <a class="" href="{{ $recent->url }}" target="_blank" rel="noopener noreferrer">
                        Visit Site
                    </a>
                </div>
                <div class="text-white text-center bg-grey-600 hover:bg-black rounded px-2 py-1 mr-2 mb-4">
                    <a class="" href="{{ $recent->code_url }}" target="_blank" rel="noopener noreferrer">
                        View Code
                    </a>
                </div>
            </div>
        </div>

        @endforeach

    </div>

</div>

@endsection