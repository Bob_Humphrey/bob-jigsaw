@extends('_layouts.master')

@section('pageTitle')
    Experience
@endsection

@section('content')

    <h3 class="h3-style mb-4">University of North Carolina Wilmington (2011-2017)</h3>

    <div class="font-serif text-grey-700 text-lg leading-normal mb-8">
    <p class="mb-4">
        For 6 years I was the Web and Applications Developer
        for <a class="blue-link" href="https://library.uncw.edu/" target="_blank rel="noopener noreferrer"">
        Randall Library</a>
        at <a class="blue-link" href="https://uncw.edu/" target="_blank" rel="noopener noreferrer">
        The University of North Carolina Wilmington</a>.
        This is a busy academic library that sees 6 to 7,000 students
        come through its doors on a daily basis,
        with several thousand more visiting its website.
    </p>
    <p>
        I was the library's only coder, and I was responsible for the full stack -- front
        end, back end, and servers. I completed
        <a class="blue-link" href="/projects">
        dozens of projects</a>
        that helped the university's students and faculty make better use of
        their library. In the process of doing this, I put together
        a pretty extensive
        <a class="blue-link" href="/skills">
        set of skills</a>.
    </p>
    </div>

    <div class="lg:flex -mx-2 ">
    <div class="lg:w-1/3 px-2 mb-3">
        <figure class="image is-square">
        <img src="/assets/images/randall/L1.jpg" alt="Randall Library">
        </figure>
    </div>
    <div class="lg:w-1/3 px-2 mb-3">
        <figure class="image is-square">
        <img src="/assets/images/randall/L5.jpg" alt="Randall Library">
        </figure>
    </div>
    <div class="lg:w-1/3 px-2 mb-3">
        <figure class="image is-square">
        <img src="/assets/images/randall/L9.jpg" alt="Randall Library">
        </figure>
    </div>
    </div>

    <div class="lg:flex -mx-2">
    <div class="lg:w-1/3 px-2 mb-3">
        <figure class="image is-square">
        <img src="/assets/images/randall/L2.jpg" alt="Randall Library">
        </figure>
    </div>
    <div class="lg:w-1/3 px-2 mb-3">
        <figure class="image is-square">
        <img src="/assets/images/randall/L6.jpg" alt="Randall Library">
        </figure>
    </div>
    <div class="lg:w-1/3 px-2 mb-3">
        <figure class="image is-square">
        <img src="/assets/images/randall/L10.jpg" alt="Randall Library">
        </figure>
    </div>
    </div>

    <div class="lg:flex -mx-2 ">
    <div class="lg:w-1/3 px-2 mb-3">
        <figure class="image is-square">
        <img src="/assets/images/randall/L3.jpg" alt="Randall Library">
        </figure>
    </div>
    <div class="lg:w-1/3 px-2 mb-3">
        <figure class="image is-square">
        <img src="/assets/images/randall/L7.jpg" alt="Randall Library">
        </figure>
    </div>
    <div class="lg:w-1/3 px-2 mb-3">
        <figure class="image is-square">
        <img src="/assets/images/randall/L11.jpg" alt="Randall Library">
        </figure>
    </div>
    </div>

    <div class="lg:flex -mx-2 mb-6">
    <div class="lg:w-1/3 px-2 mb-3">
        <figure class="image is-square">
        <img src="/assets/images/randall/L4.jpg" alt="Randall Library">
        </figure>
    </div>
    <div class="lg:w-1/3 px-2 mb-3">
        <figure class="image is-square">
        <img src="/assets/images/randall/L8.jpg" alt="Randall Library">
        </figure>
    </div>
    <div class="lg:w-1/3 px-2 mb-3">
        <figure class="image is-square">
        <img src="/assets/images/randall/L12.jpg" alt="Randall Library">
        </figure>
    </div>
    </div>

    <h3 class="h3-style mb-4">Roadway Express (1983-2007)</h3>
    <div class="font-serif text-grey-700 text-lg leading-normal mb-8">
    <p class="mb-4">
        During the years I worked at Roadway Express, it was one of the largest
        trucking companies in the country, with over 30,000 employees and more than
        600 terminals located coast to coast. (Today it is part of the trucking
        giant
        <a class="blue-link" href="http://yrc.com/" target="_blank" rel="noopener noreferrer">
        YRC Freight</a>.)
    </p>
    <p>
        Some of my titles at Roadway included Business Systems Analyst
        and Technical Team Lead.  I was responsible for determining
        business requirements and putting together technical direction for
        a group of about twelve developers, building a variety of applications for the
        company's thousand member national sales force.       
    </p>
    </div>

    <div class="lg:flex -mx-2 mb-24">
    <div class="lg:w-1/3 px-2 mb-3">
        <figure class="image is-square">
        <img src="/assets/images/rex/Rex1.jpg" alt="Randall Library">
        </figure>
    </div>
    <div class="lg:w-1/3 px-2 mb-3">
        <figure class="image is-square">
        <img src="/assets/images/rex/Rex3.jpg" alt="Randall Library">
        </figure>
    </div>
    <div class="lg:w-1/3 px-2 mb-3">
        <figure class="image is-square">
        <img src="/assets/images/rex/Rex2.jpg" alt="Randall Library">
        </figure>
    </div>
    </div>

@endsection