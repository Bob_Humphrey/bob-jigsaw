---
pagination:
  collection: projects
  perPage: 10
---

@extends('_layouts.master')

@php

@endphp

@section('pageTitle')
    Projects
@endsection

@section('content')

    <div class="mb-20">

        <p class="font-serif text-grey-700 text-lg">A partial list of some of the projects I completed 
        during the six years I worked as a web developer for 
        <a class="blue-link" href="https://library.uncw.edu/" target="_blank rel="noopener noreferrer"">
        Randall Library</a>
        at <a class="blue-link" href="https://uncw.edu/" target="_blank" rel="noopener noreferrer">
        The University of North Carolina Wilmington</a>.</p>

        <nav class="">
            @include('_partials.page-nav')
        </nav>

        <ul>
            @foreach ($pagination->items as $project)
                <li class="">
                    <div class="border border-grey-200 pt-8 md:pt-20 mb-12 overflow-hidden ">
                        <figure class="image overflow-hidden md:ml-20 md:mr-20 md:mb-20">
                            <img src="https://cockpit.bob-humphrey.com/storage/uploads{{ $project->image }}" alt="{{ $project->name }} screen capture">
                        </figure>
                        <div class="bg-grey-50 text-center p-8">
                            <h3 class="text-blue-500 font-bold text-2xl pb-4">
                                {{ $project->name }} 
                            </h3>
                            <div class="text-grey-700 text-sm pb-2">
                                {{ $project->description }} 
                            </div>
                        </div>
                    </div>
                </li>
            @endforeach
        </ul>

        <nav class="">
            @include('_partials.page-nav')
        </nav>

    </div>

@endsection