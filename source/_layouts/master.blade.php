<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="The personal website of web developer Bob Humphrey.">
        <title>
            Bob Humphrey
        </title>
        <link rel="stylesheet" href="{{ mix('css/main.css', 'assets/build') }}">
        <!-- Matomo -->
        <script type="text/javascript">
        var _paq = window._paq || [];
        /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
        _paq.push(['trackPageView']);
        _paq.push(['enableLinkTracking']);
        (function() {
            var u="//analytics.bob-humphrey.com/";
            _paq.push(['setTrackerUrl', u+'matomo.php']);
            _paq.push(['setSiteId', '1']);
            var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
            g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
        })();
        </script>
        <!-- End Matomo Code -->
    </head>
    <body>
        <div class="lg:flex ">

            <nav class="lg:w-2/5 bg-grey-300">
                @include('_partials.header')
            </nav>

            <main class="lg:w-3/5">
                <div class="pt-8 px-6 lg:px-32">
                    <h2 class="font-sans text-blue-700 text-3xl font-bold mb-8">
                        @yield('pageTitle')
                    </h2>
                    @yield('content')
                    @yield('pageDetail')
                </div>
            </main>
        </div>
        
        <footer class="lg:w-full">
            @include('_partials.footer')
        </footer>

    </body>
</html>
