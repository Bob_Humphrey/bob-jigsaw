@extends('_layouts.master')

@php

@endphp

@section('pageTitle')
    Photography
@endsection


@section('pageDetail')

<div class="mb-20">

    <figure class="image">
        <img src="https://cockpit.bob-humphrey.com/storage/uploads{{ $page->image }}" alt="{{ $page->description }}">
    </figure>

    <div class="mt-2 mb-10 font-serif text-grey-700 text-lg">
        {{ $page->description }}
    </div>

</div>


@endsection