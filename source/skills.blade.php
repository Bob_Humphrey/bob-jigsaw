@extends('_layouts.master')

@php 

    // Retrieve the data 
    $directus_token = $page->directus_token();
    $skillsCatApiUrl = 'https://directus2.bob-humphrey.com/bob/items/skill_categories?'
        . 'access_token=' 
        . $directus_token
        . '&fields=*,skills.*';
    $data = json_decode(file_get_contents($skillsCatApiUrl));
    $records=$data->data;


    // Organize the records into 3 columns
    $categoryCount = count($records);
    $floor = floor($categoryCount / 3);
    $mod = $categoryCount % 3;
    $endFirstCol = $floor;
    $endSecondCol = $floor * 2;
    if ($mod >= 1) {
        $endFirstCol++;
        $endSecondCol++;
    }
    if ($mod == 2) {
        $endSecondCol++;
    }
    $count = 0;
    $column1 = array();
    $column2 = array();
    $column3 = array();
    foreach($records as $record) {
        $count++;
        if ($count <= $endFirstCol) {
            $column1[] = $record;
        }
        elseif ($count <= $endSecondCol) {
            $column2[] = $record;
        }
        else {
            $column3[] = $record;
        }
    }

@endphp

@section('pageTitle')
    Skills
@endsection

@section('content')
    <div class="lg:flex mb-24">
        <div class="lg:w-1/3 pr-6 lg:border-r border-grey-400">
            @foreach ($column1 as $record) 
            <h3 class="h3-style mb-2">{{ $record->name }}    </h3>
                <ul class="font-serif text-grey-700 text-lg mb-4">
                @foreach($record->skills as $skill)
                    <li>{{ $skill->name }}</li>
                @endforeach
                </ul>
             @endforeach
        </div>

        <div class="lg:w-1/3 lg:pl-6 pr-6 lg:border-r border-grey-400">
            @foreach ($column2 as $record) 
            <h3 class="h3-style mb-2">{{ $record->name }}    </h3>
                <ul class="font-serif text-grey-700 text-lg mb-4">
                @foreach($record->skills as $skill)
                    <li>{{ $skill->name }}</li>
                @endforeach
                </ul>
             @endforeach
        </div>

        <div class="lg:w-1/3 lg:pl-6 pr-6">
            @foreach ($column3 as $record) 
            <h3 class="h3-style mb-2">{{ $record->name }}    </h3>
                <ul class="font-serif text-grey-700 text-lg mb-4">
                @foreach($record->skills as $skill)
                    <li>{{ $skill->name }}</li>
                @endforeach
                </ul>
             @endforeach
        </div>
    </div>
@endsection