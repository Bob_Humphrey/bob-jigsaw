@extends('_layouts.master')

@section('content')

<div class="font-serif text-grey-900 text-lg mb-20">
    <p class="mb-10 text-3xl leading-tight">
        Hi, my name is Bob. I'm a web developer with more than 30 years of coding experience. 
    </p>

    <p class="mb-10 text-2xl leading-tight">
        I build websites that are fast, secure, and easy.  Easy to use, easy to maintain, 
        and easy on the eyes.
    </p>

    <p class="mb-8">
        I can handle every phase of a web project, including
        planning, designing, coding, and testing.
        I know the front end and the back end, the client as well as the server.
    </p>

    <p class="mb-8 italic">
        I have a <a class="blue-link" href="/experience">history</a> 
        of solving problems and making customers smile!
    </p>

    <p class="mb-8">
        In my spare time, I'm a skilled 
        <a class="blue-link" href="/photography">photographer</a> and passionate
        <a class="blue-link" href="art">painter</a>.  I like to read, listen to music, and take long walks.
        Travel is an obsession and my passport is always ready to go.  I'm a huge fan
        of lifelong learning -- and dogs!
    </p>
</div>

@endsection
