---
pagination:
  collection: photos
  perPage: 20
---

@extends('_layouts.master')

@php

@endphp

@section('pageTitle')
    Photography 
@endsection

@section('content')

    <div class="mb-20">

        <nav class="lg:pr-32">
            @include('_partials.page-nav')
        </nav>

        <ul>
            @foreach ($pagination->items as $photo)
                <li class="mb-6 lg:pr-32">
                    <a href="photo/{{ $photo->sort }}">
                        <figure class="image">
                            <img src="https://cockpit.bob-humphrey.com/storage/uploads{{ $photo->image }}" alt="{{ $photo->description }}">
                        </figure>
                    </a>
                    <div class="mt-2 mb-10 font-serif text-grey-700 text-lg">
                        {{ $photo->description }}
                    </div>
                </li>
            @endforeach
        </ul>

        <nav class="lg:pr-32">
            @include('_partials.page-nav')
        </nav>

    </div>


@endsection