<div class="bg-grey-100">
    <div class=" w-full pt-6 lg:pb-10 pb-10 px-4 bg-grey-100 ">
        <a class="text-blue-900 no-underline" href="/">
            <h1 class="text-center font-sans text-3xl font-bold pt-0">Bob Humphrey</h1>
        </a>
        <div class="text-center font-sans text-2xl text-blue-500 -mt-2 mb-2 ">Web Developer</div>
        <div class="hidden lg:flex justify-center w-full lg:text-xl w-full">
            <a class="text-blue-900 hover:text-blue-500 no-underline px-2" href="/experience">
            Experience
            </a>
            <a class="text-blue-900 hover:text-blue-500 no-underline px-2" href="/work">
            Recent Work
            </a>
            <a class="text-blue-900 hover:text-blue-500 no-underline px-2" href="/projects">
            Projects
            </a>
            <a class="text-blue-900 hover:text-blue-500 no-underline px-2" href="/skills">
            Skills
            </a>
        </div>
        <div class="text-center lg:hidden">
            <ul class="">
                <li class="">
                    <a class="text-blue-900 font-bold" href="/experience">
                        EXPERIENCE
                    </a>
                </li>
                <li class="">
                    <a class="text-blue-900 font-bold" href="/work">
                        RECENT WORK
                    </a>
                </li>
                <li class="">
                    <a class="text-blue-900 font-bold" href="/projects">
                        PROJECTS
                    </a>
                </li>
                <li class="">
                    <a class="text-blue-900 font-bold" href="/skills">
                        SKILLS
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="">
        <figure class="hidden lg:block mb-8 lg:mb-0 ">
            <img src="/assets/images/dog1.png" alt="Dog Smile Factory ">
        </figure>
    </div>

</div>