<div class="flex justify-between mt-4 mb-8">
    <div class="flex justify-start"> 
        @if ($pagination->previous)
            <a class="hidden lg:block bg-grey-400 hover:bg-grey-700 text-white font-bold py-2 px-8 mr-2 font-serif font-bold no-underline rounded"
                href="{{ $page->baseUrl }}{{ $pagination->first }}">
                First
            </a>
        @else
            <div class="hidden lg:block bg-grey-200 font-serif font-bold text-white no-underline py-2 px-8 mr-2 rounded">
                First
            </div>
        @endif

        @if ($pagination->previous)
            <a class="bg-grey-400 hover:bg-grey-700 text-white font-bold py-2 px-8 mr-2 font-serif font-bold no-underline rounded"
                href="{{ $page->baseUrl }}{{ $pagination->previous }}">
                Prev
            </a>
        @else
            <div class="bg-grey-200 font-serif font-bold text-white no-underline py-2 px-8 mr-2 rounded">
                Prev
            </div>
        @endif
    </div>

    <div class="hidden lg:flex items-center justify-center flex-grow bg-grey-200 rounded">
        <div class="font-serif font-bold text-grey-700">Page {{ $pagination->currentPage }} of {{ $pagination->totalPages }} 
        </div>
    </div>

    <div class="flex justify-end">
        @if ($pagination->next)
            <a class="bg-grey-400 hover:bg-grey-700 text-white font-bold py-2 px-8 mx-2 font-serif font-bold no-underline rounded"
                href="{{ $page->baseUrl }}{{ $pagination->next }}">
                Next
            </a>
        @else
            <div class="bg-grey-200 font-serif font-bold py-2 px-8 mx-2 text-white no-underline rounded">
                Next
            </div>
        @endif

        @if ($pagination->next)
            <a class="hidden lg:block bg-grey-400 hover:bg-grey-700 text-white font-bold py-2 px-8 font-serif font-bold no-underline rounded"
                href="{{ $page->baseUrl }}{{ $pagination->last }}">
                Last
            </a>
        @else
            <div class="hidden lg:block bg-grey-200 font-serif font-bold py-2 px-8 text-white no-underline rounded">
                Last
            </div>
        @endif
    </div>
</div>