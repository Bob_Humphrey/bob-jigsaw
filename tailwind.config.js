module.exports = {
  theme: {
    extend: {},

    fontFamily: {
      sans: [
        "-apple-system",
        "BlinkMacSystemFont",
        '"Segoe UI"',
        "Roboto",
        '"Helvetica Neue"',
        "Arial",
        '"Noto Sans"',
        "sans-serif",
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
        '"Noto Color Emoji"'
      ],
      serif: [
        "roboto_slabregular",
        "Georgia",
        "Cambria",
        '"Times New Roman"',
        "Times",
        "serif"
      ],
      mono: [
        "Menlo",
        "Monaco",
        "Consolas",
        '"Liberation Mono"',
        '"Courier New"',
        "monospace"
      ]
    },

    colors: {
      "blue-900": "#0D47A1",
      "blue-800": "#1565C0",
      "blue-700": "#1976D2",
      "blue-600": "#1E88E5",
      "blue-500": "#2196F3",
      "blue-400": "#42A5F5",
      "blue-300": "#64B5F6",
      "blue-200": "#90CAF9",
      "blue-100": "#BBDEFB",

      "grey-900": "#212121",
      "grey-800": "#424242",
      "grey-700": "#616161",
      "grey-600": "#757575",
      "grey-500": "#9E9E9E",
      "grey-400": "#BDBDBD",
      "grey-300": "#E0E0E0",
      "grey-200": "#EEEEEE",
      "grey-100": "#F5F5F5",

      black: "#22292f",
      white: "#ffffff"
    }
  },
  variants: {},
  plugins: [
    require('@tailwindcss/custom-forms'),
  ]
}
